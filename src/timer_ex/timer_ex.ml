let process_event = function
  | Kqueue.Event.Timer timer -> begin
    Printf.printf
      "Timer %d fired %d times since last poll\n%!"
      timer.Kqueue.Event.Timer.id
      timer.Kqueue.Event.Timer.count
  end
  | _ ->
    print_endline "Unexpected event"

let rec kqueue_loop kq =
  let eventlist = Kqueue.Eventlist.create 5 in
  let ret =
    Kqueue.kevent
      kq
      ~changelist:Kqueue.Eventlist.null
      ~eventlist
      ~timeout:(Some (Kqueue.Timeout.create ~sec:1 ~nsec:0))
  in
  (match ret with
    | -1 ->
      Printf.printf "Error executing kqueue\n%!"
    | 0 ->
      Printf.printf "Timeout triggered\n%!"
    | _ ->
      Kqueue.Eventlist.iter
        ~f:(fun k -> process_event (Kqueue.Event.of_kevent k))
        eventlist);
  kqueue_loop kq

let rec make_timers = function
  | [] ->
    []
  | (id, sec)::ts ->
    let timer =
      Kqueue.Change.Filter.Timer.({ id = id
                                  ; unit = Unit.to_t Unit.Seconds
                                  ; time = sec
                                  })
    in
    timer::make_timers ts

let main () =
  let kq = Kqueue.create () in
  let timers = make_timers [(1, 5); (2, 10); (3, 2)] in
  let kevents =
    List.map
      (fun t ->
        Kqueue.Change.Filter.to_kevent
          Kqueue.Change.Action.(to_t [Flag.Add])
          (Kqueue.Change.Filter.Timer t))
      timers
  in
  ignore
    (Kqueue.kevent
       kq
       ~changelist:(Kqueue.Eventlist.of_list kevents)
       ~eventlist:Kqueue.Eventlist.null
       ~timeout:None);
  Printf.printf "Waiting...\n%!";
  kqueue_loop kq

let () = main ()
