let stdin = Kqueue.unsafe_int_of_file_descr Unix.stdin

let process_event = function
  | Kqueue.Event.Read {Kqueue.Event.Read.descr; len} -> begin
    let b = Bytes.create len in
    ignore (Unix.read (Kqueue.unsafe_file_descr_of_int stdin) b 0 len);
    Printf.printf "id = %d len = %d content = %s\n%!" descr len (Bytes.to_string b)
  end
  | _ ->
    print_endline "Unexpected event"

let rec loop kq =
  let eventlist = Kqueue.Eventlist.create 1 in
  let ret =
    Kqueue.kevent
      kq
      ~changelist:Kqueue.Eventlist.null
      ~eventlist
      ~timeout:(Some (Kqueue.Timeout.create ~sec:5 ~nsec:0))
  in
  (match ret with
    | -1 -> begin
      Printf.printf "Something terrible has happened\n";
      exit 1
    end
    | 0 ->
      Printf.printf "Hey buddy, say something\n%!"
    | _ ->
      Kqueue.Eventlist.iter
        ~f:(fun k -> process_event (Kqueue.Event.of_kevent k))
        eventlist);
  loop kq

let main () =
  let kq = Kqueue.create () in
  let stdin_event =
    Kqueue.Change.(
      Filter.to_kevent
        (Action.to_t [Action.Flag.Add])
        (Filter.Read stdin))
  in
  ignore
    (Kqueue.kevent
       kq
       ~changelist:(Kqueue.Eventlist.of_list [stdin_event])
       ~eventlist:Kqueue.Eventlist.null
       ~timeout:None);
  loop kq

let () = main ()
